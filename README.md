# Getting Started with Chemlab

This project is designed to help you get started using Chemlab and write your first end-to-end test using Chemlab.

Follow the steps below.  Try them on your own, or view the solution to each step.

## 1. Add gems to Gemfile

- Add `chemlab` as a dependency
- Add `chemlab-library-the-internet` as a dependency

<details>
<summary>Expand for solution</summary>

```ruby
# Gemfile

gem 'chemlab'
gem 'chemlab-library-the-internet'

group :development do
  gem 'rspec'
end
```

After Gems are added, run `$ bundle install`.  If bundler is not installed, run `$ gem install bundler` first.
</details>

## 2. Configure spec_helper.rb

The `spec_helper.rb` file will be used as our helper to configure RSpec, as well as Chemlab.  It sets up what each test
will be doing.

### 2.a. Require Chemlab and "The Internet"

<details>
<summary>Expand for solution</summary>

```ruby
# spec_helper.rb

require 'chemlab'
require 'chemlab-library-the-internet'

# 2.b. ...
```

</details>

### 2.b. Configure Chemlab

1. Configure the Base URL to be `https://the-internet.herokuapp.com`
1. Configure the Browser to be Chrome
1. Configure Chemlab to include "The Internet" as a library

<details>
<summary>Expand for solution</summary>

```ruby
# spec_helper.rb

# 2.a. ...

Chemlab.configure do |chemlab|
  chemlab.base_url = 'https://the-internet.herokuapp.com'
  chemlab.browser = :chrome
  chemlab.libraries = [TheInternet]
end
```

</details>

## 3. Write the spec

We will be testing the Login functionality in our spec. 

The login functionality is at: https://the-internet.herokuapp.com/login .

This login result has three possible permutations:

1. On a successful login, you will be redirected to https://the-internet.herokuapp.com/secure .
1. On an unsuccessful login:
  * If the username is incorrect, you will be presented with a red alert saying "Your username is invalid!"
  * If the password is incorrect, you will be presented with a red alert saying "Your password is invalid!"

Let's write a spec to test each of these possible permutations.

> **Hint**: You can browse the Page Library elements and methods at: https://gitlab-org.gitlab.io/quality/chemlab-library-the-internet/

### Correct Login

- Visit the index page
- Click the "Form Authentication" link to go the login page
- Set the username to `tomsmith`
- Set the password to `SuperSecretPassword!`
- Click the `Login` button
- Validate that you were logged in correctly

<details>
<summary>Expand for solution</summary>

```ruby
# frozen_string_literal: true

require_relative 'spec_helper'

module QA
  RSpec.describe 'The Internet' do
    using ::Chemlab::Vendor
    
    describe 'Login' do
      context 'when correct credentials used' do
        it 'logs in correctly' do
          TheInternet::Page::Index.perform do |index|
            index.visit
            index.form_authentication # https://gitlab-org.gitlab.io/quality/chemlab-library-the-internet/TheInternet/Page/Index.html#form_authentication-instance_method
          end
          
          TheInternet::Page::Login.perform do |login|
            login.username = 'tomsmith'
            login.password = 'SuperSecretPassword!'
            login.login
          end
          
          TheInternet::Page::Secure.perform do |secure|
            aggregate_failures do
              expect(secure).to be_on_page
              expect(secure).to be_visible
            end
          end
        end
      end
  
      context 'when incorrect credentials used' do
        # write the example(s)
      end
    end
  end
end
```

</details>

### Incorrect Login - Invalid Username

- Navigate to the Login page
- Set the username to `invalid`
- Set the password to `SuperSecretPassword!`
- Click the Login button
- Validate that the alert shows with the text `Your username is invalid!`

<details>
<summary>Expand for solution</summary>

```ruby
# frozen_string_literal: true

require_relative 'spec_helper'

module QA
  RSpec.describe 'The Internet' do
    using ::Chemlab::Vendor
    
    describe 'Login' do
      context 'when correct credentials used' do
        # write the example(s)
      end
  
      context 'when incorrect credentials used' do
        before do
          TheInternet::Page::Login.perform(&:visit)
        end
        
        it 'shows an error when an invalid username is used' do
          TheInternet::Page::Login.perform do |login|
            login.username = 'invalid'
            login.password = 'SuperSecretPassword!'
            login.login
          end
          
          TheInternet::Component::Flash.perform do |flash|
            aggregate_failures do
              expect(flash).to be_visible
              expect(flash.message).to include('Your username is invalid!')
            end
          end
        end
      end
    end
  end
end
```

</details>

### Incorrect Login - Invalid Password

- Navigate to the Login page
- Set the username to `tomsmith`
- Set the password to `invalid`
- Click the Login button
- Validate that the alert shows with the text `Your password is invalid!`

<details>
<summary>Expand for solution</summary>

```ruby
# frozen_string_literal: true

require_relative 'spec_helper'

module QA
  RSpec.describe 'The Internet' do
    using ::Chemlab::Vendor
    
    describe 'Login' do
      context 'when correct credentials used' do
        # write the example(s)
      end
  
      context 'when incorrect credentials used' do
        before do
          TheInternet::Page::Login.perform(&:visit)
        end
        
        it 'shows an error when an invalid password is used' do
          TheInternet::Page::Login.perform do |login|
            login.username = 'tomsmith'
            login.password = 'invalid'
            login.login
          end
          
          TheInternet::Component::Flash.perform do |flash|
            aggregate_failures do
              expect(flash).to be_visible
              expect(flash.message).to include('Your password is invalid!')
            end
          end
        end
      end
    end
  end
end
```

</details>
