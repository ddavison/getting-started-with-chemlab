# frozen_string_literal: true

# 1. Require Chemlab and "The Internet" library
# 2. Configure Chemlab
#   2.a. with a base_url of https://the-internet.heroku.app.com
#   2.b. with the Chrome browser
#   2.c. add "The Internet" as a library
